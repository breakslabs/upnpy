import ctypes.util
import fcntl
import socket
import struct

import pytest

import upnpy.network as network

TEST_IP_ADDRESS = '10.50.100.200'

@pytest.fixture
def mock_ifaddr_ioctl(monkeypatch):
    """Mock ioctl SIOCGIFADDR to return TEST_IP_ADDRESS"""
    def ifioctl(fd, code, data):
        if code == 0x8915:
            iface = struct.unpack('15s', data[:15])[0].strip(b'\x00')
            addr = socket.inet_aton(TEST_IP_ADDRESS)
            return struct.pack('20s235s', iface, addr)
        raise ValueError(f'Unknown ioctl code: 0x{code:0x}')
        # Raise exception if incorrect IOCTL called?
    monkeypatch.setattr(fcntl, 'ioctl', ifioctl)

@pytest.fixture
def mock_ifaddr_fail(monkeypatch):
    def ifioctl_OSError(fd, code, data):
        raise OSError
    monkeypatch.setattr(fcntl, 'ioctl', ifioctl_OSError)

    
def test_ip_for_iface(mock_ifaddr_ioctl):
    assert network.ip_for_iface('eth0') == TEST_IP_ADDRESS

def test_ip_for_iface_fail(mock_ifaddr_fail):
    assert network.ip_for_iface('eth0') == None
    
def test_all_ifaces():
    """Test network.all_ifaces() does not break"""
    # Not really much to test here other than "does not break"
    network.all_ifaces()

def test_all_ifaces_libc(monkeypatch):
    """Test the libc fallback for network.all_ifaces() does not break"""
    with monkeypatch.context() as m:
        m.delattr(socket, 'if_nameindex')
        network.all_ifaces()

def test_all_ifaces_no_libc(monkeypatch):
    """Test network.all_ifaces() libc fallback breaks if libc not found"""
    with monkeypatch.context() as m:
        m.delattr(socket, 'if_nameindex')
        m.setattr(ctypes.util, 'find_library', lambda l: None)
        with pytest.raises(OSError) as e:
            network.all_ifaces()
        
def test_parse_response():
    """Test network.parse_response() returns correct data"""
    x = network.parse_response(TEST_IP_ADDRESS,
        b'HTTP/1.5 300\r\nCACHE-CONTROL: max-age=100\r\nTEST: 5\r\n\r\nBODY')
    assert x.host_ip == TEST_IP_ADDRESS
    assert x.protocol == 'HTTP/1.5'
    assert x.status == '300'
    assert x.headers['CACHE-CONTROL'] == 'max-age=100'
    assert x.headers['TEST'] == '5'
    assert x.body == 'BODY'
    
