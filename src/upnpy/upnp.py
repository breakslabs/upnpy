"""
UPnP functions and classes.

.. data:: UPNP_SEARCH_MSG

    Default UPnP broadcast search payload.
"""
from collections import namedtuple
import io
import re
import urllib
import warnings

import arrow
from defusedxml import ElementTree
import requests

from . import network

UPNP_MCAST_SEARCH_MSG = \
  b'M-SEARCH * HTTP/1.1\r\n' \
  b'HOST:239.255.255.250:1900\r\n' \
  b'ST:upnp:rootdevice\r\n' \
  b'MX:2\r\n' \
  b'MAN:"ssdp:discover"\r\n' \
  b'\r\n'

_DATE_FORMATS = ['ddd, DD MMM YYYY HH:mm:ss ZZZ',]


def discover(bind=None, timeout=5, message=None):
    """Find UPnP devices and services on a given network.

    :param bind: IP address of interface to bind to, or None. Default: None
    :param timeout: Maximum time to wait for responses from hosts, in seconds.
        default: 5
    :param message: Search message to send. Uses :data:`UPNP_MCAST_SEARCH_MSG` 
        if None.
    :type bind: str
    :type timeout: int
    :type message: bytes
    :return: Iterator of responses
    :rtype: Iterator[:class:`UPnPHost`]

    Emits ``message`` via the IPv4 site-local multicast address, and
    enumerates any replies received before ``timeout`` seconds have
    elapsed. If bind is provided and is a valid IP address string, we
    will attempt to bind the outgoing socket to the network interface
    with that address. Otherwise, the request is sent out over an
    unbound interface, and it's up to the OS to determine which
    interface is used. If message is None, the default UPnP SEARCH-M
    message is sent (:data:`UPNP_SEARCH_MSG`).
    """
    if message is None:
        message = UPNP_MCAST_SEARCH_MSG
    for resp in network.broadcast_udp(message, bind, timeout):
       yield _upnphost_from_response(network.parse_response(*resp))
        

def _upnphost_from_response(response):
    """Generate a UPnPDevice instance given an M-SEARCH response."""
    max_age = None
    for cc in response.headers.get_all('cache-control'):
        match_age = re.match('max-age=([0-9]*)', cc, re.IGNORECASE)
        if match_age:
            max_age = int(match_age.group(1))
    server = response.headers.get('server')
    search_target = response.headers.get('st')
    usn = response.headers.get('usn')
    location = response.headers.get('location')
    received = response.headers.get('date')
    return UPnPHost(max_age, server, search_target, usn, location, received)


class UPnPHost:
    """UPnP host interface."""
    __slots__= ('cache_timeout', 'server', 'search_target', 'USN', 'location',
                    'received')
        
    def __init__(self, cache_timeout, server, search_target, usn, location,
                 received):
        """Initialize UPnPDevice instance, optionally from search response.

        :param cache_timeout: Cache max-age value in seconds.
        :param server: Responding server description.
        :param search_target: M-SEARCH search target.
        :param usn: Device unique service name.
        :param location: Device location URL
        :param received: M-SEARCH response timestamp
        :type cache_timeout: int
        :type server: str
        :type search_target: str
        :type usn: str
        :type location: str
        """
        self._update_from_response(cache_timeout, server, search_target, usn,
                                   location, received)
    
    def _update_from_response(self, cache_timeout, server, search_target,
                              usn, location, received):
        """Initialize attributes from a server response.

        See :method:`UPnPHost.__init__` for arguments.
        """
        self.cache_timeout = cache_timeout
        self.server = server
        self.search_target = search_target
        self.USN = usn
        self.location = urllib.parse.urlparse(location)
        if received:
            with warnings.catch_warnings():
                warnings.simplefilter('ignore')
                self.received = arrow.get(received, _DATE_FORMATS)
        else:
            self.received = arrow.get()

    @property
    def valid(self):
        return (arrow.get() - self.received).seconds < self.cache_timeout
            
    def scpd_xml(self):
        """Fetch the SCPD from the device."""
        resp = requests.get(self.location.geturl())
        resp.raise_for_status()
        xml = io.StringIO(resp.text)
        ns = dict([node for _, node in
                   ElementTree.iterparse(xml, events=['start-ns'])])
        xml.seek(0)
        return (ns, ElementTree.parse(xml))
            
    def __repr__(self):
        return (f'{self.__class__.__name__}({self.cache_timeout}, '
                f'"{self.server}", "{self.search_target}", "{self.USN}", '
                f'"{self.location.geturl()}", "{self.received.format()}")')
