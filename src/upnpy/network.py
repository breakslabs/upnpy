""" Architecture-specific base-level network functions.

.. warning::

    **Currently only tested and supported on GNU/Linux**

    This module attempts to use Python network interfaces where possible, but
    will most likely break severely on non-GNU/Linux operating
    systems. Specifically, :func:`all_ifaces` attempts to use
    :func:`socket.if_nameindex` if present, but will fall back to the libc
    implementation if not. I have no idea if :func:`socket.if_namindex` works
    under, e.g., Windows, but I'm fairly certain the libc fallback will break
    spectacularly.

    In addition, the :func:`ip_for_iface` function relies on an ioctl
    (specifically SIOCGIFADDR, which is not defined in :mod:`socket` or
    :mod:`fcntl`), and expects the appropriate return structure. I'm fairly
    certain this is specific to Linux and will probably break even on other
    unices. If there's a better way to do this, I'm all earholes.

    It should be possible to refactor this such that other methods can be
    invoked for other operating systems, should that be desireable.  
"""

from collections import namedtuple
import fcntl
import http.client 
import io
import socket
import struct


class BindError(Exception):
    pass


HTTPResponse = namedtuple('HTTPResponse', ['host_ip', 'protocol',
                                               'status', 'headers',
                                               'body'])
HTTPResponse.host_ip.__doc__ = 'The IP address of the responding host, as str'
HTTPResponse.protocol.__doc__ = 'The response protocol (e.g. "HTTP/1.1").'
HTTPResponse.status.__doc__ = 'The HTTP status response as a string.'
HTTPResponse.headers.__doc__ = ('The response headers as a '
                                ':class:`http.client.HTTPMessage` object.')
HTTPResponse.body.__doc__ = 'The body of the response as bytes.'


def make_udp_endpoint(host=None, bind=None, timeout=5):
    """Create a UDP endpoint.

    :param host: Remote host IP address. If None, uses the network
        multicast address ('239.255.255.250').
    :param bind: IP address of network interface to bind to. If None, no
        binding is performed, and the OS is responsible for selecting
        the interface to bind to. Useful for devices with multiple
        network interfaces connected to distinct networks.
    :param timeout: Socket timeout value, in seconds. Default is 5. If
        None, no timeout is set.
    :type host: str
    :type bind: str
    :type timeout: int
    :returns: Configured datagram socket ready to send and receive data.
    :rtype: :class:`socket.socket`

    Prepare a UDP Datagram socket for sending and receiving data. By default,
    configures the socket to bind to the default interface, as determined by
    the operating system, and connected to the network multicast address,
    with a send/receive timeout of five seconds.
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    if timeout is not None:
        sock.settimeout(timeout)
    if(bind):
        try:
            sock.bind((bind, 0))
        except OSError:
            raise BindError(f'Cannot bind to interface with address {bind}')
    return sock

def make_mcast_listener(bind=None):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)#, socket.IPPROTO_UDP)
    try:
        sock.bind((bind, 1900))
    except OSError as e:
        raise BindError(f'Cannot bind to interface {bind} on port 1900 {e}')
    return sock

def broadcast_udp(msg, bind=None, timeout=5): #pragma: no coverage
    """Send broadcast message over UDP.

    :param msg: Message to send.
    :param bind: Optional network to bind to.
    :param timeout: Time to wait for responses, in seconds. Default: 5
    :type msg: bytes
    :type bind: string
    :type timeout: int
    :return: All responses from any valid recipients.
    :rtype: Iterator[(str, bytes)]
    
    Send a binary message over UDP to the network broadcast address 
    (239.255.255.250). An optional network address may be provided, and
    we will attempt to bind to the interface with that address before
    transmitting.
    """
    # Set up UDP socket
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    s.settimeout(timeout)
    if(bind):
        try:
            s.bind((bind, 0))
        except OSError:
            raise BindError(f'Cannot bind to interface with address {bind}')
    s.sendto(msg, ('239.255.255.250', 1900) )

    try:
        while True:
            data, addr = s.recvfrom(65507)
            yield (addr, data)
    except socket.timeout:
        pass

async def async_broadcast_udp(msg, bind=None, timeout=5):
    async for res in broadcast_udp(msg, bind, timeout):
        yield res
    
def all_ifaces():
    """Return all network interfaces and their indexes.
    
    :return: List of network interface indexes and names.
    :rtype: list of tuples (int index, str name)
    :raises: OSError on various issues.

    Prefers socket.if_nameindex() where present, but attempts to use the
    libc version of same when the socket library function is not available.
    """
    try:
        return socket.if_nameindex()
    except AttributeError:
        # Support for other operating systems should insert here.
        # For now, we support POSIX/libc implementations. Probably.
        return _libc_if_nameindex()

def _libc_if_nameindex(): 
    """Return interface indexes and names using libc's if_nameindex()

    :return: List of network interface indexes and names.
    :rtype: list of tuples (int index, str name)
    :raises: OSError is libc not found

    Almost certainly POSIX only, and only if libc supports if_nameindex().
    """
    import ctypes
    import ctypes.util
    from itertools import takewhile
    
    class IFace(ctypes.Structure):
        _fields_ = [('idx', ctypes.c_int), ('name', ctypes.c_char_p)]
        
    libc_s = ctypes.util.find_library('c')
    if libc_s is None:
        raise OSError('Could not locate libc')
    libc = ctypes.cdll.LoadLibrary(libc_s)
    if_nameindex = libc.if_nameindex
    if_nameindex.restype = ctypes.POINTER(IFace)
    c_ifaces = if_nameindex()
    ifaces = [(x.idx, x.name)
                  for x in takewhile(lambda i: i.idx != 0, c_ifaces)]
    libc.if_freenameindex(c_ifaces)
    return ifaces


def ip_for_iface(iface):
    """Return the IP address associated with an interface.

    :param iface: The interface to query
    :type iface: str
    :return: IP address associated with interface
    :rtype: str
    :raises: OSError if given interface has no IP address.
    """
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        return socket.inet_ntoa(fcntl.ioctl(
            s.fileno(),
            0x8915,  # SIOCGIFADDR
            struct.pack('256s', iface[:15].encode("UTF-8"))
            )[20:24])
    except OSError:
        return None


def parse_response(source, response):
    """Parse a binary string as an HTTP response.
    
    :param source: IP address of the responding entity.
    :param response: The raw response data to parse.
    :type source: str
    :type response: bytes
    :return: HTTP header data and body.
    :rtype: :class:`HTTPResponse`
    
    Given the source IP address and a binary data string, return
    an HTTPResponse object with the following attributes:
    
        host_ip: The responding host's IP address as a string.
        protocol: The repsponse protocol as a string (e.g. "HTTP/1.1").
        status: The HTTP response status as a string.
        headers: HTTP headers as an :class:`http.client.HTTPMessage` object.
        body: The response body as bytes.
    """
    header, body = response.split(b'\r\n\r\n', 1)
    f = io.BytesIO(header)
    requestline = f.readline().strip().split(b' ')
    protocol, status = requestline[:2]
    headers = http.client.parse_headers(f)
    #resp = utllib3.HTTPResponse('', headers=headers)
    return HTTPResponse(source,
                        protocol.decode('utf-8'),
                        status.decode('utf-8'),
                        headers,
                        body.decode('utf-8'))
