"""MiniSSDPd support.

Provide support functions and asyncio protocol class for interfacing with
the MiniSSDP daemon.
"""

import asyncio


def decode(msg):
    """MiniSSDP response decoder.

    :param msg: Response message from MiniSSDP.
    :type msg: bytes
    :returns: Tuple containing the first decoded string in msg, and the
        remainder of the message, if any, else the empty string.
    :rtype: (str, str)
    :raises: :class:`ValueError` if msg is not at least two bytes long or
        is not a bytes instance.

    Given a response message from MiniSSDP, decode the length of the first
    string in the message, and return the string and the remainder of the 
    message.
    """
    if len(msg) < 2:
        raise ValueError('A valid MiniSSDP response must contain at '
                         'least two bytes')
    if not isinstance(msg, bytes):
        raise ValueError("Response message must be of type 'bytes'")
    mpos = 0
    mlen = 0
    while msg[mpos] & 0x80:
        mlen = (mlen<<7) | (msg[mpos]&0x7f)
        mlen += 1
    mlen = (mlen<<7) | (msg[mpos]&0x7f)
    mlen += 1
    return msg[mpos+1:mpos+mlen], msg[mpos+mlen:]

def decode_all(msg):
    """Decode and return all strings from a MiniSSDP response message.
    
    :param msg: Response message from MiniSSDP.
    :type msg: str
    :returns: List of strings decoded from the response message.
    :rtype: list
    :raises: See :func:`decode`
    """
    vals = []
    while len(msg) > 0:
        val, msg = decodelen(msg)
        vals.append(val)
    return vals

class MiniSSDPListener(asyncio.Protocol):
    """
    MiniSSDP unix socket protocol.

    Asyncio protocol object for MiniSSDP unix socket interface.

    """
    def __init__(self, loop):
        self.loop = loop

    def connection_made(self, transport):
        print('LISTENER: Connection made')
        self.transport = transport
        self.emit_request_all()

    def emit_request_all(self):
        self.transport.write(b'\x03'+(b'0x00'*2047))
        l.call_later(10, lambda: lprotocol.emit_request_all())
        
    def data_received(self, data):
        result_count = data[0]
        print(f'LISTENER: Record Count - {result_count}')
        results = decode_all(data[1:])
        for x in range(result_count):
            print('   ', SSDPRec(*results[x*3:(x*3)+3]))
        print()
        
    def connection_lost(self, err):
        print(f'LISTENER: Lost connection {err}')

    def __getattribute__(self, attr):
        print(f'LISTENER: Accessing protocol attr: {attr}')
        return super(AnnounceListener, self).__getattribute__(attr)

