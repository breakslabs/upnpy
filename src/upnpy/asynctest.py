import asyncio
from collections import namedtuple
import pathlib
import socket
import struct

from upnpy import network
from upnpy import minissdpd

MINISSDPD_SOCK = pathlib.Path('/var/run/minissdpd.sock')

UPNP_MCAST_SEARCH_MSG = \
  b'M-SEARCH * HTTP/1.1\r\n' \
  b'HOST:239.255.255.250:1900\r\n' \
  b'ST:upnp:rootdevice\r\n' \
  b'MX:2\r\n' \
  b'MAN:"ssdp:discover"\r\n' \
  b'USER-AGENT: GNULinux/4.16 UPnP/1.1 UPnPy/0.1.0' \
  b'\r\n'

UPNP_UCAST_SEARCH_MSG = \
  'M-SEARCH * HTTP/1.1\r\n' \
  'HOST:{host}:{port}\r\n' \
  'ST:upnp:rootdevice\r\n' \
  'MAN:"ssdp:discover"\r\n' \
  'USER-AGENT: GNULinux/4.16 UPnP/1.1 UPnPy/0.1.0' \
  '\r\n'

BROWSE_TEST = """
<?xml version="1.0"?>
<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
  <s:Body>
    <u:Browse xmlns:u="urn:schemas-upnp-org:service:ContentDirectory:1">
      <ObjectID>0</ObjectID>
      <BrowseFlag>BrowseDirectChildren</BrowseFlag>
      <Filter>*</Filter>
      <StartingIndex>0</StartingIndex>
      <RequestedCount>0</RequestedCount>
      <SortCriteria></SortCriteria>
    </u:Browse>
  </s:Body>
</s:Envelope>
"""

SSDPRec = namedtuple('SSDPDRec', ['url', 'st', 'usn'])

class MSearchProtocol(asyncio.DatagramProtocol):
    def __init__(self, loop):
        self.loop = loop
    
    def connection_made(self, transport):
        print("Connection made")
        transport.sendto(UPNP_MCAST_SEARCH_MSG, ('239.255.255.250', 1900))
        #msg = UPNP_UCAST_SEARCH_MSG.format(host='10.130.2.1', port=1900)
        #print(f'MSG: {msg}')
        #transport.sendto(msg.encode(), ('10.130.2.1', 1900))
        
        
    def datagram_received(self, data, addr):
        #print("Received:", data, "from", addr)
        pass
    
    def connection_lost(self, exc):
        print("Connection lost")
        self.loop.stop()

    def error_received(self, err):
        print("Error received:", err)

    def __getattribute__(self, attr):
        print(f'Accessing protocol attr: {attr}')
        return super(MSearchProtocol, self).__getattribute__(attr)



    
if __name__ == '__main__':
    l = asyncio.get_event_loop()
    #s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    #s.settimeout(5)
    #s.bind(('10.130.2.24', 0))
    sock = network.make_udp_endpoint(bind='10.130.2.24')
    co = l.create_datagram_endpoint(lambda: MSearchProtocol(l), sock=sock)
    if MINISSDPD_SOCK.exists():
        li = l.create_unix_connection(lambda: AnnounceListener(l),
                                      path=str(MINISSDPD_SOCK))
    transport, protocol = l.run_until_complete(co)
    ltransport, lprotocol = l.run_until_complete(li)
    l.run_forever()
    transport.close()
    ltransport.close()
    l.close()
