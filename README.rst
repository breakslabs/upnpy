UPnPy
-----

Quick and Dirty Overview
========================

Ultimately this will be a collection of utilities to interact with
various UPnP and DLNA devices. Specifically this was written to
scratch an itch caused by a lack of Linux/Unix DLNA console
utilities. I use a combination of Media Player Daemon and UPMPD as
media renderers, and MiniDLNA for media servers. Both work well for my
purposes, but I have not been so happy with the availibility of media
controllers for the desktop/server. This code is intended to provide a
solid basis for filling that void, providing console tools and also
acting as a library for other applications like web- and desktop-based
GUIs.

At present, both the UPnP code and the DLNA code will reside in the
single package. As the code grows, I may decide is makes more sense to
segment the functionality into separate packages.

Installation
============
Recommend installing in a virtual environment. Setup is via the usual
setuptools procedure::

    python ./setup.py install

or::

    python ./setup.py develop

    
Note
~~~~

If you get an error during installation of 'python-dateutils' like::
  
    UserWarning: Unknown distribution option: 'use_scm_version'

You should update your setuptools::
      
    pip install --upgrade setuptools
